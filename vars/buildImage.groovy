#!/usr/bin/env groovy

def call(){
    echo" building the docker image"
    withCredentials([usernamePassword(credentialsId:'nexus-docker-repo',passwordVariable:'PASS',usernameVariable:'USER')]){
        sh "docker build -t 34.206.164.133:8083/visaimg:v$currentVersion .  "
        sh "echo $PASS | docker login -u $USER --password-stdin 34.206.164.133:8083"
        sh "docker push 34.206.164.133:8083/visaimg:v$currentVersion "
    }
}


