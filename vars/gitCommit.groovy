#!/usr/bin/env groovy
def call(){
  withCredentials([usernamePassword(credentialsId:'gitlab-cred',passwordVariable:'PASS',usernameVariable:'USER')]){
    sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/anuppoudel54/visa-docker-build.git"
    sh 'git add .'
    sh 'git commit -m "ci:version increment"'
    sh 'git push origin HEAD:main'
  }
   
}
